#pragma once

#include "stdio.h"
#include "string.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_event.h"
#include "driver/gpio.h"
#include "esp_timer.h"
#include "esp_ota_ops.h"
#include "esp_sleep.h"
#include "zh_espnow.h"
#include "zh_network.h"
#include "zh_ds18b20.h"
#include "zh_dht.h"
#include "zh_config.h"

#ifdef CONFIG_NETWORK_TYPE_DIRECT
#define zh_send_message(a, b, c) zh_espnow_send(a, b, c)
#define ZH_EVENT ZH_ESPNOW
#else
#define zh_send_message(a, b, c) zh_network_send(a, b, c)
#define ZH_EVENT ZH_NETWORK
#endif

#ifdef CONFIG_IDF_TARGET_ESP8266
#define ZH_CHIP_TYPE HACHT_ESP8266
#elif CONFIG_IDF_TARGET_ESP32
#define ZH_CHIP_TYPE HACHT_ESP32
#elif CONFIG_IDF_TARGET_ESP32S2
#define ZH_CHIP_TYPE HACHT_ESP32S2
#elif CONFIG_IDF_TARGET_ESP32S3
#define ZH_CHIP_TYPE HACHT_ESP32S3
#elif CONFIG_IDF_TARGET_ESP32C2
#define ZH_CHIP_TYPE HACHT_ESP32C2
#elif CONFIG_IDF_TARGET_ESP32C3
#define ZH_CHIP_TYPE HACHT_ESP32C3
#elif CONFIG_IDF_TARGET_ESP32C6
#define ZH_CHIP_TYPE HACHT_ESP32C6
#endif

#ifdef CONFIG_IDF_TARGET_ESP8266
#define ZH_CPU_FREQUENCY CONFIG_ESP8266_DEFAULT_CPU_FREQ_MHZ;
#define get_app_description() esp_ota_get_app_description()
#else
#define ZH_CPU_FREQUENCY CONFIG_ESP_DEFAULT_CPU_FREQ_MHZ;
#define get_app_description() esp_app_get_description()
#endif

#define ZH_SENSOR_ATTRIBUTES_MESSAGE_FREQUENCY 60

#define ZH_MESSAGE_TASK_PRIORITY 2
#define ZH_MESSAGE_STACK_SIZE 2048

typedef struct sensor_config_t // Structure of data exchange between tasks, functions and event handlers.
{
    zh_sensor_hardware_config_message_t hardware_config; // Storage structure of sensor hardware configuration data.
    volatile bool gateway_is_available;                  // Gateway availability status flag. Used to control the tasks when the gateway connection is established/lost. Used only when external powered.
    uint8_t gateway_mac[ESP_NOW_ETH_ALEN];               // Gateway MAC address. Used only when external powered.
    uint8_t sent_message_quantity;                       // System counter for the number of sended messages. Used only when powered by battery.
    zh_dht_handle_t dht_handle;                          // Unique DTH11/22 sensor handle.
    TaskHandle_t attributes_message_task;                // Unique task handle for zh_send_sensor_attributes_message_task(). Used only when external powered.
    TaskHandle_t status_message_task;                    // Unique task handle for zh_send_sensor_status_message_task(). Used only when external powered.
    const esp_partition_t *update_partition;             // Unique handle for next OTA update partition. Used only when external powered.
    esp_ota_handle_t update_handle;                      // Unique handle for OTA functions. Used only when external powered.
    uint16_t ota_message_part_number;                    // System counter for the number of received OTA messages. Used only when external powered.
} sensor_config_t;

// Function for loading the sensor hardware configuration from NVS memory.
static void zh_load_config(sensor_config_t *sensor_config);
// Function for saving the sensor hardware configuration to NVS memory.
static void zh_save_config(const sensor_config_t *sensor_config);

// Function for loading the power selection GPIO number from NVS memory.
static uint8_t zh_load_power_selection_pin(void);
// Function for saving the power selection GPIO number to NVS memory.
static void zh_save_power_selection_pin(const uint8_t *power_selection_pin);

// Function for GPIO and sensor initialization.
static void zh_sensor_init(sensor_config_t *sensor_config);
// Function for sending sensor data to the gateway and putting the module into deep sleep. Used only when powered by battery.
static void zh_sensor_deep_sleep(sensor_config_t *sensor_config);

// Function for prepare the hardware configuration message and sending it to the gateway.
static void zh_send_sensor_hardware_config_message(const sensor_config_t *sensor_config);
// Task for prepare the attributes message and sending it to the gateway.
static void zh_send_sensor_attributes_message_task(void *pvParameter);
// Function for prepare the configuration message and sending it to the gateway.
static uint8_t zh_send_sensor_config_message(const sensor_config_t *sensor_config);
// Task for prepare the status message and sending it to the gateway.
static void zh_send_sensor_status_message_task(void *pvParameter);

// Function for ESP-NOW event processing.
static void zh_espnow_event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);